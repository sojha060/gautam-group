<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// admin auth

Route::group(['prefix' => 'admin'], function () {
    Route::get('/login', 'auth\AdminLoginController@showLoginForm');
    Route::post('/login', 'auth\AdminLoginController@login')->name('admin.login');
});

// admin routes

Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
    Route::get('/', 'back\admin\HomeController@dashboard')->name('admin.dashboard');
    Route::post('/logout', 'auth\AdminLoginController@logout')->name('admin.logout');;

});



// seller auth
Route::group(['prefix' => 'seller'], function () {
    Route::get('/login', 'auth\SellerLoginController@showLoginForm');
    Route::post('/login', 'auth\SellerLoginController@login')->name('seller.login');

    // register
    Route::get('/register', 'auth\SellerRegisterController@showRegisterForm')->name('seller.register');
    Route::post('/register', 'auth\SellerRegisterController@register')->name('seller.register');;
});

// seller routes

Route::group(['prefix' => 'seller', 'middleware' => ['seller']], function () {

    Route::get('/', 'back\seller\HomeController@dashboard')->name('seller.dashboard');
    Route::post('/logout', 'auth\SellerLoginController@logout')->name('seller.logout');;


});
