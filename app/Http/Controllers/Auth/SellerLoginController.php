<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class SellerLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest:seller', ['except' => 'logout']);
    }




    public function showLoginForm()
    {
        return view('auth.seller_login');
    }

    public function login(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ];

        $request->validate($rules);

        // attempt to log the user in
        $login = Auth::guard('seller')->attempt(
            ['email' => $request->email, 'password' => $request->password],
            $request->remember
        );

        // dd($login);

        if ($login) {
            // dd("ok logged in");
            // if successful then redirect to their intended location
            return redirect()->intended(route('seller.dashboard'));
        }

        $validator = Validator::make([], []);
        $validator->getMessageBag()->add('email', 'These credentials do not match our records');
        return redirect()->back()->withInput($request->only('email'))->withErrors($validator);;
    }


   // logout for visitor
   public function logout(Request $request)
   {
       $this->guard()->logout();
       $request->session()->invalidate();

       return redirect()->route('seller.login');
   }
}
