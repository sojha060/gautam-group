<?php

namespace App\Http\Controllers\back\seller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function dashboard()
    {
        return view('backend.seller.dashboard');
    }

}
