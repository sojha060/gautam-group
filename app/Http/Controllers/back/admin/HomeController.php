<?php

namespace App\Http\Controllers\back\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function dashboard(){

        return view('backend.admin.dashboard');
    }


}
